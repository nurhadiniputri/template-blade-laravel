@extends('layout.master')
@section('judul')
    <h1>Buat Account Baru</h1>
@endsection
@section('subjudul')
    <h2 class="card-title">Sign Up Form</h2>
@endsection
@section('isi')
    <form action="/welcome" method="POST">
        @csrf
            <label>First name:</label><br><br>
                <input type="text" name="firstname"><br><br>
            <label>Last name:</label><br><br>
                <input type="text" name="lastname"><br><br>
            <label>Gender:</label><br><br>
                <input type="radio" name="gender"> Man<br>
                <input type="radio" name="gender"> Woman<br>
                <input type="radio" name="gender"> Other<br><br>
            <label>Nationality:</label><br><br>
                <select value="nationality">
                    <option value="1">Indonesian</option>
                    <option value="2">Singaporean</option>
                    <option value="3">Malaysian</option>
                    <option value="4">Australian</option>
                </select><br><br>
            <label>Language Spoken:</label><br><br>
                <input type="checkbox"> Bahasa Indonesia<br>
                <input type="checkbox"> English<br>
                <input type="checkbox"> Japanese<br>
                <input type="checkbox"> Other<br><br>
            <label>Bio:</label><br><br>
                <textarea name="bio" cols="40" rows="10"></textarea><br><br>
            <button type="submit">Sign Up</button>
    </form>
@endsection
@section('sidebar')
    <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Utama</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/register" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Register</p>
                </a>
              </li>
            </ul>
          </li>
         
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>
            </ul>
          </li>
@endsection