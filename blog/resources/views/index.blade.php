@extends('layout.master')
@section('judul')
    <h1>Halaman Utama</h1>
@endsection
@section('subjudul')
    <h1 class="card-title"><b>SanberBook</b></h1>
@endsection
@section('isi')
        <h4>Social Media Developer Santai Berkualitas</h4>
            <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
        <h5>Benefit Join di SanberBook</h5>
            <ul>
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        <h5>Cara Bergabung ke SanberBook</h5>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
@endsection
@section('sidebar')
    <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Utama</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/register" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Register</p>
                </a>
              </li>
            </ul>
          </li>
         
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>
            </ul>
          </li>
@endsection