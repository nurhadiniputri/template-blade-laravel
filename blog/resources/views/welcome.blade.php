@extends('layout.master')
@section('judul')
    <h1>Halaman Welcome</h1>
@endsection
@section('subjudul')
    <h2 class="card-title">SELAMAT DATANG {{$depan}} {{$belakang}}</h2>
@endsection
@section('isi')
    <p>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</p>
@endsection
@section('sidebar')
    <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Utama</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/register" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Halaman Register</p>
                </a>
              </li>
            </ul>
          </li>
         
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>
            </ul>
          </li>
@endsection
